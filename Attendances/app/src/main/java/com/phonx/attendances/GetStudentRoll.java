package com.phonx.attendances;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class GetStudentRoll extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_student_roll);
        EditText etRoll=findViewById(R.id.rollnumber);
        Button btnGo=findViewById(R.id.go);
        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //check for empty roll number
                if (etRoll.getText().toString().equals(""))
                {
                    Toast.makeText(GetStudentRoll.this, "Please enter roll number", Toast.LENGTH_SHORT).show();
                }else
                {
                    String roll=etRoll.getText().toString();
                    Attenandence.roll_no=roll;
                    startActivity(new Intent(GetStudentRoll.this,Attenandence.class));
                    finish();
                }
            }
        });
    }
}