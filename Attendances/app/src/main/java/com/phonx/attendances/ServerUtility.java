package com.phonx.attendances;

public class ServerUtility {
    public static final String SERVER_URL="http://192.168.0.103:8084/AttendanceBOTServer/";
    public static String get_student_info_url()
    {
        return  SERVER_URL+"GetStudentInfo";
    }

    public static String get_subject_info_url()
    {
        return  SERVER_URL+"GetSubjectinfo";
    }



    public static String get_student_attendace_url()
    {
        return  SERVER_URL+"GetStudentAttendance";
    }
}
