package com.phonx.attendances;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Attenandence extends AppCompatActivity {
    TextView txtstuname, txtpname, txtpnum, txtroll, txtclass, txtyear, tv4, tv1, tv11, tv12, tv2, tv21, tv22, tv23, tv31, tv33, tv3;
    TableRow tr;
    Spinner spinner;
    BarChart barChart;
    public static double science, scienceAtt, english, englishAtt, math, mathAtt, avg;

    public static String roll_no = "";
    public static String month = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attenandence);
        initGUI();
        loadStudentInfo();

    }


    private void initGUI() {

        txtstuname = findViewById(R.id.txtstuname);
        txtpname = findViewById(R.id.txtpname);
        txtpnum = findViewById(R.id.txtpnum);
        txtroll = findViewById(R.id.txtroll);
        txtclass = findViewById(R.id.txtclass);
        txtyear = findViewById(R.id.txtyear);
        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);
        tv3 = findViewById(R.id.tv3);
        tv4 = findViewById(R.id.tv4);
        tv11 = findViewById(R.id.tv11);
        tv12 = findViewById(R.id.tv12);
        tv21 = findViewById(R.id.tv21);
        tv22 = findViewById(R.id.tv22);
        tv31 = findViewById(R.id.tv31);
        tv33 = findViewById(R.id.tv33);
         barChart = (BarChart) findViewById(R.id.barchart);

        spinner = findViewById(R.id.spinner1);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                month = parent.getItemAtPosition(position).toString();
                loadSubjectinfo();
                loadStudentattendace();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void loadStudentInfo() {
        //volley request
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ServerUtility.get_student_info_url(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    String studentName = json.getString("name");
                    String parentname = json.getString("parentname");
                    String clas = json.getString("class");
                    String year = json.getString("year");
                    String mobile = json.getString("parentMobile");
                    txtstuname.setText(studentName);
                    txtroll.setText(roll_no);
                    txtpname.setText(parentname);
                    txtpnum.setText(mobile);
                    txtclass.setText(clas);
                    txtyear.setText(year);


                } catch (JSONException e) {
                    Toast.makeText(Attenandence.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    Toast.makeText(Attenandence.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("studentRoll", roll_no);
                params.put("month", month);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    private void loadSubjectinfo() {

        //volley request
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ServerUtility.get_subject_info_url(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    if (json.has("success")) {

                        tv2.setText("English");
                        tv1.setText("Science");
                        tv3.setText("Maths");
                        String SciLectures = json.getString("scilec");
                        String EngLectures = json.getString("englec");
                        String MathLectures = json.getString("mathlec");


                        tv11.setText(SciLectures);
                        tv21.setText(EngLectures);
                        tv31.setText(MathLectures);

                        science = Double.parseDouble(SciLectures);
                        english = Double.parseDouble(EngLectures);
                        math = Double.parseDouble(MathLectures);

                        tv4.setVisibility(View.VISIBLE);
                        barChart.setVisibility(View.VISIBLE);
                    }else
                    {
                        Toast.makeText(Attenandence.this, "No Record found", Toast.LENGTH_SHORT).show();
                        tv2.setText("English");
                        tv1.setText("Science");
                        tv3.setText("Maths");
                        tv11.setText("");
                        tv21.setText("");
                        tv31.setText("");
                        tv12.setText("");
                        tv22.setText("");
                        tv33.setText("");
                        tv4.setVisibility(View.GONE);
                        barChart.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    Toast.makeText(Attenandence.this, "error in get subject info", Toast.LENGTH_SHORT).show();
                    Toast.makeText(Attenandence.this, e.toString(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    Toast.makeText(Attenandence.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("month", month);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    private void loadBarchart() {




        ArrayList<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry((float) scienceAtt, 0));
        entries.add(new BarEntry((float) englishAtt, 1));
        entries.add(new BarEntry((float) mathAtt, 2));
//        entries.add(new BarEntry(2f, 1));
//        entries.add(new BarEntry(5f, 2));
//        entries.add(new BarEntry(20f, 3));
//        entries.add(new BarEntry(15f, 4));
//        entries.add(new BarEntry(19f, 5));

        BarDataSet bardataset = new BarDataSet(entries, "Cells");

        ArrayList<String> labels = new ArrayList<String>();
//        labels.add("2016");
//        labels.add("2015");
//        labels.add("2014");
//        labels.add("2013");
//        labels.add("2012");
//        labels.add("2011");
        labels.add("Science");
        labels.add("English");
        labels.add("Maths");


        BarData data = new BarData(labels, bardataset);
        barChart.setData(data); // set the data and list of labels into chart
        barChart.setDescription("Attendace");  // set the description
        bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
        barChart.animateY(500);
    }


    private void loadStudentattendace() {
        //volley request
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ServerUtility.get_student_attendace_url(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);

                    String SciLecturesAtt = json.getString("SciLecturesAtt");
                    String EngLecturesAtt = json.getString("EngLecturesAtt");
                    String MathLecturesAtt = json.getString("MathLecturesAtt");

                    tv12.setText(SciLecturesAtt);
                    tv22.setText(EngLecturesAtt);
                    tv33.setText(MathLecturesAtt);

                    scienceAtt = Double.parseDouble(SciLecturesAtt);

                    englishAtt = Double.parseDouble(EngLecturesAtt);

                    mathAtt = Double.parseDouble(MathLecturesAtt);
                    avg = ((scienceAtt + englishAtt + mathAtt) / (science + english + math)) * 100;
                    String a = String.format("%.2f", avg);
                    Toast toast = Toast.makeText(getApplicationContext(), "Avg=" + a, Toast.LENGTH_SHORT);
                    tv4.setText("Avg=" + a);
                    if (avg > 75) {
                        tv4.setBackgroundColor(Color.GREEN);
                    } else if (avg > 60) {
                        tv4.setBackgroundColor(Color.YELLOW);
                    } else {
                        tv4.setBackgroundColor(Color.RED);
                    }
                    loadBarchart();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    Toast.makeText(Attenandence.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("studentRoll", roll_no);
                params.put("month", month);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
