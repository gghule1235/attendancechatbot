-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2021 at 11:23 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `attendancebot`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_attendance`
--

CREATE TABLE `tbl_attendance` (
  `attendace_pk_id` int(11) NOT NULL,
  `studentID` int(11) NOT NULL,
  `subjectName` varchar(45) NOT NULL,
  `totalLectures` int(11) NOT NULL,
  `lectureAttend` int(11) NOT NULL,
  `attendaceStatus` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_studentinfo`
--

CREATE TABLE `tbl_studentinfo` (
  `student_pk_id` int(11) NOT NULL,
  `studentName` varchar(100) NOT NULL,
  `parentName` varchar(100) NOT NULL,
  `rollNo` int(11) NOT NULL,
  `studentClass` varchar(45) NOT NULL,
  `studentYear` varchar(45) NOT NULL,
  `studentAddress` varchar(200) NOT NULL,
  `parentMobile` varchar(45) NOT NULL,
  `studentStatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_studentinfo`
--

INSERT INTO `tbl_studentinfo` (`student_pk_id`, `studentName`, `parentName`, `rollNo`, `studentClass`, `studentYear`, `studentAddress`, `parentMobile`, `studentStatus`) VALUES
(1, 'Dinesh Shantaram Ubale', 'Shantaram Raghunath Ubale', 10, 'Computer and Science Engineering ', '1', 'Wagholi, Pune', '7350456969', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_studentinfo`
--
ALTER TABLE `tbl_studentinfo`
  ADD PRIMARY KEY (`student_pk_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_studentinfo`
--
ALTER TABLE `tbl_studentinfo`
  MODIFY `student_pk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
